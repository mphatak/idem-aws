import tempfile
from unittest import mock

import dict_tools.data
import pytest
import yaml


@pytest.mark.asyncio
async def test_gather_config(hub, mock_hub):
    """
    No region in acct, verify that it is collected from config
    """
    CONFIG = b"""
    acct:
      extras:
        aws:
          region_name: mock-region
    """

    ctx = dict_tools.data.NamespaceDict(acct={})

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {"aws": {"region_name": "mock-region"}}

    ctx = dict_tools.data.NamespaceDict(acct={})
    session = mock.MagicMock()
    hub.tool.boto3.session.get = mock_hub.tool.boto3.session.get
    hub.tool.boto3.session.get.return_value = session
    await hub.tool.boto3.client.get_client(ctx, None)
    session.client.assert_called_once_with(
        service_name=None,
        region_name="mock-region",
        api_version=None,
        use_ssl=True,
        endpoint_url=None,
        aws_access_key_id=None,
        aws_secret_access_key=None,
        aws_session_token=None,
        verify=None,
    )


@pytest.mark.asyncio
async def test_gather_esm_config(hub, mock_hub):
    """
    Verify that ESM region_name can come from esm config if none is specified in profile
    """
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "acct": {
                "extras": {
                    "aws": {
                        "region_name": "config-region",
                        "esm": {"region_name": "config-esm-region"},
                    }
                }
            }
        }
    )
    ctx = dict_tools.data.NamespaceDict(acct={})
    try:
        await hub.esm.aws.enter(ctx)
    except:
        ...

    # The ctx should have been modified with the esm region name
    assert ctx.acct.region_name == "config-esm-region"

    ctx = dict_tools.data.NamespaceDict(acct={})
    session = mock.MagicMock()
    hub.tool.boto3.session.get = mock_hub.tool.boto3.session.get
    hub.tool.boto3.session.get.return_value = session
    await hub.tool.boto3.client.get_client(ctx, None)
    session.client.assert_called_once_with(
        service_name=None,
        region_name="config-region",
        api_version=None,
        use_ssl=True,
        endpoint_url=None,
        aws_access_key_id=None,
        aws_secret_access_key=None,
        aws_session_token=None,
        verify=None,
    )


@pytest.mark.asyncio
async def test_gather_esm_general_config(hub, mock_hub):
    """
    Verify that ESM region_name can come from general config if none is specified in profile
    """
    hub.OPT = dict_tools.data.NamespaceDict(
        {"acct": {"extras": {"aws": {"region_name": "config-region"}}}}
    )
    ctx = dict_tools.data.NamespaceDict(acct={})
    try:
        await hub.esm.aws.enter(ctx)
    except:
        ...

    # The ctx should have been modified with the esm region name
    assert ctx.acct.region_name == "config-region"

    ctx = dict_tools.data.NamespaceDict(acct={})
    session = mock.MagicMock()
    hub.tool.boto3.session.get = mock_hub.tool.boto3.session.get
    hub.tool.boto3.session.get.return_value = session
    await hub.tool.boto3.client.get_client(ctx, None)
    session.client.assert_called_once_with(
        service_name=None,
        region_name="config-region",
        api_version=None,
        use_ssl=True,
        endpoint_url=None,
        aws_access_key_id=None,
        aws_secret_access_key=None,
        aws_session_token=None,
        verify=None,
    )


@pytest.mark.asyncio
async def test_gather_esm_credentials(hub, mock_hub):
    """
    Verify that ESM region_name from the credentials profile is not overridden by config
    """
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "acct": {
                "extras": {
                    "aws": {
                        "region_name": "config-region",
                        "esm": {"region_name": "config-esm-region"},
                    }
                }
            }
        }
    )
    ctx = dict_tools.data.NamespaceDict(acct={"region_name": "acct-region"})
    try:
        await hub.esm.aws.enter(ctx)
    except:
        ...

    # The ctx should have been modified with the esm region name
    assert ctx.acct.region_name == "acct-region"

    ctx = dict_tools.data.NamespaceDict(acct={"region_name": "acct-region"})
    session = mock.MagicMock()
    hub.tool.boto3.session.get = mock_hub.tool.boto3.session.get
    hub.tool.boto3.session.get.return_value = session
    await hub.tool.boto3.client.get_client(ctx, None)
    session.client.assert_called_once_with(
        service_name=None,
        region_name="acct-region",
        api_version=None,
        use_ssl=True,
        endpoint_url=None,
        aws_access_key_id=None,
        aws_secret_access_key=None,
        aws_session_token=None,
        verify=None,
    )
