from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_iam_group, __test):
    global PARAMETER
    ctx["test"] = __test
    group = aws_iam_group.get("group_name")
    PARAMETER["name"] = group
    PARAMETER["group"] = group

    policy_arn = hub.tool.aws.arn_utils.build(
        service="iam",
        account_id="aws",
        resource="policy/ReadOnlyAccess",
    )

    PARAMETER["policy_arn"] = policy_arn

    ret = await hub.states.aws.iam.group_policy_attachment.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.iam.group_policy_attachment '{PARAMETER['group']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            f"Created aws.iam.group_policy_attachment '{PARAMETER['group']}'"
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["group"] == resource.get("group")
    assert PARAMETER["policy_arn"] == resource.get("policy_arn")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.iam.group_policy_attachment.describe(ctx)
    PARAMETER["resource_id"]
    group = PARAMETER["group"]
    policy_arn = PARAMETER["policy_arn"]
    resource_key = f"{group}/{policy_arn}"
    assert resource_key in describe_ret
    # Verify that the describe output format is correct
    assert "aws.iam.group_policy_attachment.present" in describe_ret[resource_key]
    described_resource = describe_ret[resource_key].get(
        "aws.iam.group_policy_attachment.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_group_policy_attachment(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="test_exec_get", depends=["present"])
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.iam.group_policy_attachment.get(
        ctx=ctx,
        name=PARAMETER["name"],
        group=PARAMETER["group"],
        policy_arn=PARAMETER["policy_arn"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_group_policy_attachment(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.group_policy_attachment.absent(
        ctx,
        name=PARAMETER["name"],
        group=PARAMETER["group"],
        policy_arn=PARAMETER["policy_arn"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_group_policy_attachment(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.iam.group_policy_attachment",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.iam.group_policy_attachment",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.group_policy_attachment.absent(
        ctx,
        name=PARAMETER["name"],
        group=PARAMETER["group"],
        policy_arn=PARAMETER["policy_arn"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.iam.group_policy_attachment",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


def assert_group_policy_attachment(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("group") == resource.get("group")
    assert parameters.get("resource_id") == resource.get("resource_id")
    assert parameters.get("policy_arn") == resource.get("policy_arn")
