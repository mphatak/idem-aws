import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_web_acl(hub, ctx):
    # web acl is not supported by localstack and so skipping localstack pipeline.
    if hub.tool.utils.is_running_localstack(ctx):
        return

    web_acl_name = "idem-test-web-acl-" + str(uuid.uuid4())
    scope = "REGIONAL"
    default_action = {"Allow": {}}
    visibility_config = {
        "SampledRequestsEnabled": True,
        "CloudWatchMetricsEnabled": False,
        "MetricName": "test_metric_waf",
    }
    description = "create waf to protect your web applications from common web exploits"
    rules = [
        {
            "Name": "AWS-AWSManagedRulesBotControlRuleSet",
            "Priority": 1,
            "Statement": {
                "ManagedRuleGroupStatement": {
                    "VendorName": "AWS",
                    "Name": "AWSManagedRulesBotControlRuleSet",
                }
            },
            "OverrideAction": {"None": {}},
            "VisibilityConfig": {
                "SampledRequestsEnabled": True,
                "CloudWatchMetricsEnabled": False,
                "MetricName": "AWS-AWSManagedRulesBotControlRuleSet",
            },
        }
    ]
    custom_response_bodies = {
        "success": {
            "ContentType": "APPLICATION_JSON",
            "Content": '{\n "success": "created resource"\n}',
        }
    }
    captcha_config = {"ImmunityTimeProperty": {"ImmunityTime": 1000}}
    tags = [{"Key": "resource", "Value": "WAF"}]

    # Create web acl with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.wafv2.web_acl.present(
        test_ctx,
        name=web_acl_name,
        scope=scope,
        default_action=default_action,
        visibility_config=visibility_config,
        description=description,
        rules=rules,
        custom_response_bodies=custom_response_bodies,
        captcha_config=captcha_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Would create aws.wafv2.web_acl {web_acl_name}" in ret["comment"]
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert_web_acl(
        resource,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    # Create web acl in real
    ret = await hub.states.aws.wafv2.web_acl.present(
        ctx,
        name=web_acl_name,
        scope=scope,
        default_action=default_action,
        visibility_config=visibility_config,
        description=description,
        rules=rules,
        custom_response_bodies=custom_response_bodies,
        captcha_config=captcha_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created '{web_acl_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert_web_acl(
        resource,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    # Describe web acl
    describe_ret = await hub.states.aws.wafv2.web_acl.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.wafv2.web_acl.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.wafv2.web_acl.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags,
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            described_resource_map.get("tags")
        ),
    )
    assert_web_acl(
        described_resource_map,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    description = "update waf test"
    custom_response_bodies = {
        "success": {
            "ContentType": "APPLICATION_JSON",
            "Content": '{\n "success": "created resource updated"\n}',
        }
    }
    captcha_config = {"ImmunityTimeProperty": {"ImmunityTime": 5000}}
    tags = [{"Key": "resource", "Value": "WAF"}, {"Key": "target", "Value": "app sync"}]

    # update web acl with test flag
    ret = await hub.states.aws.wafv2.web_acl.present(
        test_ctx,
        name=web_acl_name,
        scope=scope,
        resource_id=resource_id,
        default_action=default_action,
        visibility_config=visibility_config,
        description=description,
        rules=rules,
        custom_response_bodies=custom_response_bodies,
        captcha_config=captcha_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert_web_acl(
        resource,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    default_action = {"Block": {}}
    visibility_config = {
        "SampledRequestsEnabled": True,
        "CloudWatchMetricsEnabled": False,
        "MetricName": "test_metric_waf",
    }

    # update web acl in real
    ret = await hub.states.aws.wafv2.web_acl.present(
        ctx,
        name=web_acl_name,
        scope=scope,
        resource_id=resource_id,
        default_action=default_action,
        visibility_config=visibility_config,
        description=description,
        rules=rules,
        custom_response_bodies=custom_response_bodies,
        captcha_config=captcha_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert_web_acl(
        resource,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    # Deleting tags from web acl
    tags = [tags[0]]
    ret = await hub.states.aws.wafv2.web_acl.present(
        ctx,
        name=web_acl_name,
        scope=scope,
        resource_id=resource_id,
        default_action=default_action,
        visibility_config=visibility_config,
        description=description,
        rules=rules,
        custom_response_bodies=custom_response_bodies,
        captcha_config=captcha_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )

    # Delete web acl with test flag
    ret = await hub.states.aws.wafv2.web_acl.absent(
        test_ctx, name=web_acl_name, resource_id=resource_id, scope=scope
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.wafv2.web_acl '{web_acl_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert_web_acl(
        resource,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    # Delete web acl in real
    ret = await hub.states.aws.wafv2.web_acl.absent(
        ctx, name=web_acl_name, resource_id=resource_id, scope=scope
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.wafv2.web_acl '{web_acl_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert_web_acl(
        resource,
        web_acl_name,
        scope,
        default_action,
        visibility_config,
        description,
        rules,
        custom_response_bodies,
        captcha_config,
    )

    # Delete already deleted web acl
    ret = await hub.states.aws.wafv2.web_acl.absent(
        ctx, name=web_acl_name, resource_id=resource_id, scope=scope
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.wafv2.web_acl '{web_acl_name}' already absent" in ret["comment"]


def assert_web_acl(
    resource,
    web_acl_name,
    scope,
    default_action,
    visibility_config,
    description,
    rules,
    custom_response_bodies,
    captcha_config,
):
    assert web_acl_name == resource.get("name")
    assert scope == resource.get("scope")
    assert default_action == resource.get("default_action")
    assert visibility_config == resource.get("visibility_config")
    assert description == resource.get("description")
    assert rules == resource.get("rules")
    assert custom_response_bodies == resource.get("custom_response_bodies")
    assert captcha_config == resource.get("captcha_config")
