import copy

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_config_recorder_status(hub, ctx, aws_config_recorder_delivery_channel):
    config_recorder_name = aws_config_recorder_delivery_channel.get("name")
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # skip config recorder status with test flag
    ret = await hub.states.aws.config.config_recorder_status.present(
        test_ctx, name=config_recorder_name
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would skip aws.config.config_recorder_status '{config_recorder_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert config_recorder_name == resource.get("name")

    # update config recorder status with test flag
    ret = await hub.states.aws.config.config_recorder_status.present(
        test_ctx, name=config_recorder_name, recording=True
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would update aws.config.config_recorder_status '{config_recorder_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert config_recorder_name == resource.get("name")

    # update config recorder status in real
    ret = await hub.states.aws.config.config_recorder_status.present(
        ctx, name=config_recorder_name, recording=True
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Updated aws.config.config_recorder_status '{config_recorder_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert config_recorder_name == resource.get("name")

    # reset config recorder status with test flag
    ret = await hub.states.aws.config.config_recorder_status.absent(
        test_ctx, name=config_recorder_name, resource_id=config_recorder_name
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would reset aws.config.config_recorder_status '{config_recorder_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert config_recorder_name == resource.get("name")

    # reset config recorder status in real
    ret = await hub.states.aws.config.config_recorder_status.absent(
        ctx, name=config_recorder_name, resource_id=config_recorder_name
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Rested aws.config.config_recorder_status '{config_recorder_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert config_recorder_name == resource.get("name")

    # already absent
    ret = await hub.states.aws.config.config_recorder_status.absent(
        ctx, name=config_recorder_name
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.config.config_recorder_status '{config_recorder_name}' already absent"
        in ret["comment"]
    )
