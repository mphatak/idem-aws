import copy
import uuid

import pytest


@pytest.mark.asyncio
async def test_dhcp_option(hub, ctx):
    # Create dhcp_option
    name = "idem-test-dhcp-option-" + str(uuid.uuid4())
    # tags can be passed as list or dict
    tags = {"Name": name}
    dhcp_configurations = [{"Key": "domain-name-servers", "Values": ["10.2.5.1"]}]
    # Create dhcp options with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.dhcp_option.present(
        test_ctx,
        name=name,
        dhcp_configurations=dhcp_configurations,
        vpc_id=None,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.ec2.dhcp_option '{name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert tags == resource.get("tags")
    assert resource.get("dhcp_configurations") == dhcp_configurations

    # Create real dhcp options
    ret = await hub.states.aws.ec2.dhcp_option.present(
        ctx, name=name, dhcp_configurations=dhcp_configurations, vpc_id=None, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.ec2.dhcp_option '{name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert tags == resource.get("tags")
    assert resource.get("dhcp_configurations") == dhcp_configurations

    # test describe dhcp
    describe_ret = await hub.states.aws.ec2.dhcp_option.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.dhcp_option.present"
    )

    new_tags = [
        {"Key": "Name_update", "Value": name},
    ]
    new_tags_dict = hub.tool.aws.tag_utils.convert_tag_list_to_dict(new_tags)
    # Update dhcp options with test flag
    ret = await hub.states.aws.ec2.dhcp_option.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        dhcp_configurations=dhcp_configurations,
        vpc_id=None,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"aws.ec2.dhcp_option '{name}' already exists" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_tags_dict == resource.get("tags")
    assert resource.get("dhcp_configurations") == dhcp_configurations

    # Update real dhcp options
    ret = await hub.states.aws.ec2.dhcp_option.present(
        ctx,
        name=name,
        resource_id=resource_id,
        dhcp_configurations=dhcp_configurations,
        vpc_id=None,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"aws.ec2.dhcp_option '{name}' already exists" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_tags_dict == resource.get("tags")
    assert resource.get("dhcp_configurations") == dhcp_configurations

    # Delete dhcp with test flag
    ret = await hub.states.aws.ec2.dhcp_option.absent(
        test_ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Would delete aws.ec2.dhcp_option '{name}'" in ret["comment"]

    # Delete real dhcp
    ret = await hub.states.aws.ec2.dhcp_option.absent(
        ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Deleted aws.ec2.dhcp_option '{name}'" in ret["comment"]
    resource = ret.get("old_state")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_tags_dict == resource.get("tags")
    assert resource.get("dhcp_configurations") == dhcp_configurations

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.ec2.dhcp_option.absent(
        ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.dhcp_option", name=name
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_dhcp_option_absent_with_none_resource_id(hub, ctx):
    name = "idem-test-dhcp-option-" + str(uuid.uuid4())
    # Delete dhcp option with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.dhcp_option.absent(ctx, name=name, resource_id=None)
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.dhcp_option", name=name
        )[0]
        in ret["comment"]
    )
