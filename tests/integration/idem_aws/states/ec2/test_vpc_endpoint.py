import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_vpc_endpoint(hub, ctx, aws_ec2_vpc, aws_ec2_subnet):
    vpc_endpoint_name = "idem-test-vpc-endpoint" + str(uuid.uuid4())
    tags = {"Name": vpc_endpoint_name}

    vpc_id = aws_ec2_vpc.get("VpcId")
    region_name = ctx.acct.get("region_name", "us-west-1")

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # idem state --test create
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        test_ctx,
        name=vpc_endpoint_name,
        vpc_id=vpc_id,
        tags=tags,
        service_name=f"com.amazonaws.{region_name}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.ec2.vpc_endpoint '{vpc_endpoint_name}' and attach to vpc '{vpc_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")

    assert vpc_endpoint_name == resource.get("name")
    # real create
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        ctx,
        name=vpc_endpoint_name,
        vpc_id=vpc_id,
        tags=tags,
        service_name=f"com.amazonaws.{region_name}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created aws.ec2.vpc_endpoint: '{vpc_endpoint_name}' attached to vpc: '{vpc_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert vpc_endpoint_name == resource.get("name")

    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    # describe
    describe_ret = await hub.states.aws.ec2.vpc_endpoint.describe(ctx)
    assert resource_id in describe_ret

    described_resource = describe_ret.get(resource_id).get(
        "aws.ec2.vpc_endpoint.present"
    )

    described_resource_map = dict(ChainMap(*described_resource))

    assert "vpc_id" in described_resource_map
    assert "resource_id" in described_resource_map
    assert "tags" in described_resource_map
    assert "service_name" in described_resource_map
    assert "vpc_endpoint_type" in described_resource_map
    assert "private_dns_enabled" in described_resource_map

    # idem state --test update add subnet and tags
    new_tag = {"Foo": "Bar"}
    new_tags = tags
    new_tags.update(new_tag)
    subnet_ids = [aws_ec2_subnet.get("SubnetId")]
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        test_ctx,
        name=vpc_endpoint_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        service_name=f"com.amazonaws.{region_name}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would succeed in modifying aws.ec2.vpc_endpoint '{vpc_endpoint_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert subnet_ids == resource.get("subnet_ids")

    # real update add subnet and tags
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        ctx,
        name=vpc_endpoint_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        service_name=f"com.amazonaws.{region_name}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Succeeded modifying aws.ec2.vpc_endpoint '{vpc_endpoint_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert subnet_ids == resource.get("subnet_ids")

    # idem state --test update remove subnet and tags
    new_tags = tags
    subnet_ids = []
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        test_ctx,
        name=vpc_endpoint_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        service_name=f"com.amazonaws.{region_name}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would succeed in modifying aws.ec2.vpc_endpoint '{vpc_endpoint_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tag.items() not in resource.get("tags").items()
    assert not resource.get("subnet_ids")

    # real update remove subnet and tags
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        ctx,
        name=vpc_endpoint_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        service_name=f"com.amazonaws.{region_name}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Succeeded modifying aws.ec2.vpc_endpoint '{vpc_endpoint_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tag.items() not in resource.get("tags").items()
    assert not resource.get("subnet_ids")

    # idem state --test delete
    ret = await hub.states.aws.ec2.vpc_endpoint.absent(
        test_ctx, name=vpc_endpoint_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.vpc_endpoint '{vpc_endpoint_name}'" in ret["comment"]

    old_state = ret["old_state"]
    assert vpc_id == old_state["vpc_id"]
    assert resource_id == old_state["resource_id"]
    assert new_tags == resource.get("tags")
    assert f"com.amazonaws.{region_name}.s3" == old_state["service_name"]
    assert "Interface" == old_state["vpc_endpoint_type"]
    assert not old_state["private_dns_enabled"]

    # delete real
    ret = await hub.states.aws.ec2.vpc_endpoint.absent(
        ctx, name=vpc_endpoint_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted '{vpc_endpoint_name}'" in ret["comment"]

    # delete already deleted resource
    ret = await hub.states.aws.ec2.vpc_endpoint.absent(
        ctx, name=vpc_endpoint_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    # the endpoint can be completely gone since it only stays in the "deleted" stage for a few seconds
    assert (
        f"aws.ec2.vpc_endpoint '{vpc_endpoint_name}' already absent" in ret["comment"]
    )
