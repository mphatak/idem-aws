import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False, "LocalStack does not have support for ECR repository policies"
)
@pytest.mark.asyncio
async def test_repository_policy(hub, ctx, aws_ecr_repository):
    repository_policy_temp_name = "idem-test-repository-policy-" + str(uuid.uuid4())
    registry_id = aws_ecr_repository.get("registry_id")
    repository_name = aws_ecr_repository.get("repository_name")
    resource_id = f"{registry_id}-{repository_name}"
    policy_text = (
        '{"Statement": [{"Action": "ecr:DescribeImages", "Effect": "Allow", "Principal": "*", "Sid": "'
        + repository_policy_temp_name
        + '"}], "Version": "2012-10-17"}'
    )
    policy_text_standardised = hub.tool.aws.state_comparison_utils.standardise_json(
        policy_text
    )
    new_policy_text = (
        '{"Statement": [{"Action": "ecr:DescribeImages", "Effect": "Allow", "Principal": "*", "Sid": "new-'
        + repository_policy_temp_name
        + '"}], "Version": "2012-10-17"}'
    )
    new_policy_text_standardised = hub.tool.aws.state_comparison_utils.standardise_json(
        new_policy_text
    )

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create ecr repository policy with test flag
    ret = await hub.states.aws.ecr.repository_policy.present(
        test_ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
        policy_text=policy_text,
        registry_id=registry_id,
        force=True,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.ecr.repository_policy", name=repository_policy_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert repository_policy_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert registry_id == resource.get("registry_id")
    assert repository_name == resource.get("repository_name")
    assert policy_text_standardised == resource.get("policy_text")

    # Create ecr repository policy
    ret = await hub.states.aws.ecr.repository_policy.present(
        ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
        policy_text=policy_text,
        registry_id=registry_id,
        force=True,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.ecr.repository_policy", name=repository_policy_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert repository_policy_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert registry_id == resource.get("registry_id")
    assert repository_name == resource.get("repository_name")
    assert policy_text_standardised == resource.get("policy_text")

    # Verify that the created ecr repository policy is present (describe)
    ret = await hub.states.aws.ecr.repository_policy.describe(ctx)

    assert resource_id in ret
    assert "aws.ecr.repository_policy.present" in ret.get(resource_id)
    resource = ret.get(resource_id).get("aws.ecr.repository_policy.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("name")
    assert resource_id == resource_map.get("resource_id")
    assert registry_id == resource_map.get("registry_id")
    assert repository_name == resource_map.get("repository_name")
    assert policy_text_standardised == resource_map.get("policy_text")

    # Create ecr repository policy again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.ecr.repository_policy.present(
        test_ctx,
        name=repository_policy_temp_name,
        resource_id=resource_id,
        repository_name=repository_name,
        policy_text=policy_text,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"aws.ecr.repository_policy '{repository_policy_temp_name}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create ecr repository policy again with same resource_id and no change in state
    ret = await hub.states.aws.ecr.repository_policy.present(
        ctx,
        name=repository_policy_temp_name,
        resource_id=resource_id,
        repository_name=repository_name,
        policy_text=policy_text,
        registry_id=registry_id,
        force=True,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"aws.ecr.repository_policy '{repository_policy_temp_name}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update ecr repository policy with test flag
    ret = await hub.states.aws.ecr.repository_policy.present(
        test_ctx,
        name=repository_policy_temp_name,
        resource_id=resource_id,
        repository_name=repository_name,
        policy_text=new_policy_text,
        registry_id=registry_id,
        force=True,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"aws.ecr.repository_policy '{repository_policy_temp_name}' already exists"
        in ret["comment"]
    )
    assert (
        f"Would update policy_text for aws.ecr.repository_policy '{repository_policy_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_policy_text_standardised == resource.get("policy_text")

    # Update ecr repository policy
    ret = await hub.states.aws.ecr.repository_policy.present(
        ctx,
        name=repository_policy_temp_name,
        resource_id=resource_id,
        repository_name=repository_name,
        policy_text=new_policy_text,
        registry_id=registry_id,
        force=True,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"aws.ecr.repository_policy '{repository_policy_temp_name}' already exists"
        in ret["comment"]
    )
    assert (
        f"Updated aws.ecr.repository_policy '{repository_policy_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_policy_text_standardised == resource.get("policy_text")

    # Search for existing ecr repository policy
    ret = await hub.states.aws.ecr.repository_policy.search(
        ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert repository_policy_temp_name == resource.get("name")
    assert resource_id == resource_map.get("resource_id")
    assert registry_id == resource_map.get("registry_id")
    assert repository_name == resource_map.get("repository_name")
    assert policy_text_standardised == resource_map.get("policy_text")

    # Search for non-existing ecr repository policy
    fake_repository_temp_name = "fake-" + str(uuid.uuid4())
    ret = await hub.states.aws.ecr.repository_policy.search(
        ctx,
        name=repository_policy_temp_name,
        repository_name=fake_repository_temp_name,
        registry_id=registry_id,
    )

    assert not ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.ecr.repository_policy resource for repository '{registry_id}/{fake_repository_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Delete ecr repository policy with test flag
    ret = await hub.states.aws.ecr.repository_policy.absent(
        test_ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.ecr.repository_policy", name=repository_policy_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete ecr repository policy
    ret = await hub.states.aws.ecr.repository_policy.absent(
        ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ecr.repository_policy", name=repository_policy_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete the same ecr repository policy again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.ecr.repository_policy.absent(
        ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ecr.repository_policy", name=repository_policy_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete ecr repository policy with no resource_id will consider it as absent
    ret = await hub.states.aws.ecr.repository_policy.absent(
        ctx,
        name=repository_policy_temp_name,
        repository_name=repository_name,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ecr.repository_policy", name=repository_policy_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
